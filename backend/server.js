/* eslint no-console: "off" */

const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const socket = require('socket.io');
const cors = require('cors');
const initialData = require('./db/InitialData');
const routes = require('./routes');
const attachments = require('./routes/attachments');
const AuthController = require('./auth/AuthController');
const VerifyToken = require('./auth/VerifyToken');
const VerifySocketToken = require('./socket/auth/VerifyToken');
const socketRegistry = require('./socket/socketRegistry').socketRegistryMiddleware;
const config = require('./config');

const app = express();

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

const port = normalizePort(process.env.PORT || '3001');

app.set('port', port);

const connectDb = () => {
  const options = {
    useNewUrlParser: true,
  };
  mongoose.connect(`mongodb://${config.DATABASE_HOST_URL}/${config.DATABASE_NAME}`, options, () => {
    module.exports.gridfs = require('mongoose-gridfs')({
      collection: 'attachments',
      model: 'Attachment',
      mongooseConnection: mongoose.connection
    });

    initialData.init();
  });
  mongoose.set('debug', true);
  mongoose.set('useCreateIndex', true);
  return mongoose.connection;
};

const startServer = () => {
  const server = app.listen(port);
  const io = socket.listen(server, {
    origins: [config.CLIENT_URL]
  });

  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(express.static(path.join(__dirname, 'public')));
  app.use((req, res, next) => {
    req.io = res.io = io;
    next();
  });
  app.use('/api/auth', AuthController);
  app.use('/api/attachments', attachments);
  app.use('/api', VerifyToken, routes);

  io.use(VerifySocketToken);
  io.use(socketRegistry);

  io.on('connection', (socket) => {
    socket.on('chat message', (msg) => {
      console.log(`message: ${msg}`);
    });

    socket.emit('test', 'hi');
  });

  console.log(`App started on port ${port}`);
};

connectDb()
  .on('error', console.log)
  .on('disconnected', connectDb)
  .once('open', startServer);
