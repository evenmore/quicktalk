const mongoose = require('mongoose');

const { Schema } = mongoose;

const ChatSchema = new Schema({
  invitation: { type: String, required: true },
  name: { type: String, required: true },
  members: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  lastMessage: {
    message: { type: Schema.Types.ObjectId, ref: 'Message' },
    sender: { type: Schema.Types.ObjectId, ref: 'User' }
  }
});

module.exports = mongoose.model('Chat', ChatSchema);
