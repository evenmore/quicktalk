const mongoose = require('mongoose');

const { Schema } = mongoose;

const MessageSchema = new Schema({
  time: { type: Date, required: true },
  content: { type: String },
  chat: { type: Schema.Types.ObjectId, ref: 'Chat', required: true },
  sender: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  attachments: [{ type: Schema.Types.ObjectId }]
});

module.exports = mongoose.model('Message', MessageSchema);
