const { ObjectID } = require('mongodb');
const bcrypt = require('bcryptjs');
const User = require('../../backend/models/user');
const Chat = require('../../backend/models/chat');
const Message = require('../../backend/models/message');

const users = [
  {
    _id: new ObjectID('4eb6e7e7e9b7f4194e000004'),
    login: 'rore',
    password: bcrypt.hashSync('12345', 8),
  },
  {
    _id: new ObjectID('4eb6e7e7e9b7f4194e000005'),
    login: 'olko',
    password: bcrypt.hashSync('pass', 8),
  },
  {
    _id: new ObjectID('4eb6e7e7e9b7f4194e000006'),
    login: 'nios',
    password: bcrypt.hashSync('1pass1', 8),
  },
];

const chats = [
  {
    invitation: 'affasfs',
    _id: new ObjectID('4eb6e7e7e9b7f4194e000007'),
    name: 'Chat1',
    members: [users[0]._id, users[1]._id, users[2]._id],
    lastMessage: {
      message: '4eb6e7e7e9b7f4194e000001',
      sender: users[0]._id
    }
  },
  {
    invitation: 'affasfs',
    _id: new ObjectID('4eb6e7e7e9b7f4194e000008'),
    name: 'Chat2',
    members: [users[0]._id, users[1]._id, users[2]._id],
    lastMessage: {
      message: '4eb6e7e7e9b7f4194e000002',
      sender: users[1]._id
    }
  },
];

const messages = [
  {
    _id: new ObjectID('4eb6e7e7e9b7f4194e000001'),
    time: new Date(),
    content: 'Hi, guys',
    chat: chats[0]._id,
    sender: users[0]._id,
  },
  {
    _id: new ObjectID('4eb6e7e7e9b7f4194e000003'),
    time: new Date(),
    content: 'I loveeee Mongo',
    chat: chats[0]._id,
    sender: users[2]._id,
  },
  {
    _id: new ObjectID('4eb6e7e7e9b7f4194e000002'),
    time: new Date() - 1000,
    content: 'Hola',
    chat: chats[1]._id,
    sender: users[1]._id,
  },
];

module.exports = {
  init() {
    users.forEach((user) => {
      User.findOne({ login: user.login }, (err, res) => {
        if (res) {
          return;
        }
        new User(user).save();
      });
    });

    chats.forEach((chat) => {
      Chat.findOne({ name: chat.name }, (err, res) => {
        if (res) {
          return;
        }

        new Chat(chat).save();
      });
    });

    messages.forEach((message) => {
      Message.findById(message._id, (err, res) => {
        if (res) {
          return;
        }

        new Message(message).save();
      });
    });
  },
};
