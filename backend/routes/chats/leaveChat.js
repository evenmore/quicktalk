const { ObjectID } = require('mongodb');
const Chat = require('../../models/chat');
const Message = require('../../models/message');

module.exports = (req, res) => {
  const { chatId } = req.params;

  Chat.findById(chatId).exec((err, chat) => {
    if (err || !chat) {
      return res.status(400).send(`Could not find chat by chat id: ${chatId}`);
    }

    if (!chat.members.some(member => member.equals(new ObjectID(req.userId)))) {
      return res.status(500).send('User is not in this chat yet');
    }

    const index = chat.members.indexOf(req.userId);
    if (index !== -1) {
      chat.members.splice(index, 1);
    }

    if (!chat.members || !chat.members.length) {
      // Removing all messages in this chat by current logged in user (sender)
      Message.deleteMany({ chat: chatId }, ((error) => {
        if (error) {
          return res.status(500).send(`Could not remove messages for chat: ${chatId} and user ${req.userId}`);
        }
      }));

      // Removing chat if there is no members
      chat.remove((error) => {
        if (error) {
          return res.status(500).send('Could not remove chat');
        }

        res.json({
          message: 'Chat was successfully deleted'
        });
      });
    } else {
      chat.save((error, updatedChat) => {
        if (error) {
          return res.status(500).send('Could not remove user from the chat');
        }

        res.json({
          chat: updatedChat.id
        });
      });
    }
  });
};
