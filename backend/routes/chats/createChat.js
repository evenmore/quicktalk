const uniqid = require('uniqid');
const Chat = require('../../models/chat');

module.exports = (req, res) => {
  const { userId } = req;
  const chatName = req.body.name;

  Chat.create({
    invitation: uniqid(),
    name: chatName,
    members: [userId]
  }, (err, chat) => {
    if (err) {
      return res.status(500).send(err);
    }

    chat.populate('members', 'login', (err, chat) => {
      res.json(chat);
    });
  });
};
