const messages = require('express').Router();
const sendMessage = require('./sendMessage');
const getAllMessagesForChat = require('./getAllMessagesForChat');

messages.get('/:chatId/messages', getAllMessagesForChat);
messages.post('/:chatId/messages', sendMessage);

module.exports = messages;
