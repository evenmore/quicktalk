const Message = require('../../../models/message');
const Chat = require('../../../models/chat');
const { socketRegistry } = require('../../../socket/socketRegistry');

module.exports = (req, res) => {
  const { chatId } = req.params;
  const currentUserId = req.userId;
  const { message, attachments } = req.body;

  Chat.findOne({ _id: chatId, members: currentUserId }).exec((err, chat) => {
    if (err || !chat) {
      return res.status(400).send(`Could not find chat ${chatId} with member: ${currentUserId}`);
    }

    Message.create({
      time: new Date(),
      content: message,
      chat: chatId,
      sender: currentUserId,
      attachments: attachments || []
    }, (err, message) => {
      if (err) {
        return res.status(500).send('Could not save message');
      }

      chat.lastMessage = {
        message: message.id,
        sender: currentUserId
      };
      chat.save((error) => {
        if (error) {
          return res.status(500).send(`Could not update chat ${chatId}`);
        }

        message.populate('sender', 'login', (err, message) => {
          // broadcast message to all chat members
          chat.members.forEach((member) => {
            const socketIds = socketRegistry[member.toString()];
            if (socketIds) {
              socketIds.forEach(socketId => req.io.to(socketId).emit('new-message', message));
            }
          });

          res.json(message);
        });
      });
    });
  });
};
