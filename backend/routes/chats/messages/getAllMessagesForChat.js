const Message = require('../../../models/message');
const Chat = require('../../../models/chat');

const DEFAULT_LIMIT = 50;

module.exports = (req, res) => {
  const { chatId } = req.params;
  const currentUserId = req.userId;
  const { limit, time } = req.query;
  const timestamp = time ? new Date(time) : new Date();

  Chat.findOne({ _id: chatId, members: currentUserId }).exec((err, chat) => {
    if (err || !chat) {
      return res.status(400).send(`Could not find chat ${chatId} with member: ${currentUserId}`);
    }

    Message.find({ chat: chatId, time: { $lt: timestamp } })
      .populate('sender', 'login')
      .sort('-time')
      .limit(+limit || DEFAULT_LIMIT)
      .exec((err, messages) => {
        if (err) {
          return res.status(500).send(err);
        }

        res.json(messages.sort((a,b) => a.id.localeCompare(b.id)));
      });
  });
};
