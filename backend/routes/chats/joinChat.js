const { ObjectID } = require('mongodb');
const Chat = require('../../models/chat');

module.exports = (req, res) => {
  const { invitation } = req.params;

  Chat.findOne({ invitation }).exec((err, chat) => {
    if (err || !chat) {
      return res.status(400).send(`Could not find chat for invitation: ${invitation}`);
    }

    if (chat.members.some(member => member.equals(new ObjectID(req.userId)))) {
      return res.status(500).send('User is already in this chat');
    }

    chat.members.push(req.userId);
    chat.save((err, chat) => {
      if (err) {
        return res.status(500).send('Could not add user to chat');
      }

      res.json({
        chat: chat.id
      });
    });
  });
};
