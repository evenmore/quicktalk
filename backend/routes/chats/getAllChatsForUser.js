const Chat = require('../../models/chat');

module.exports = (req, res) => {
  Chat.find({ members: req.userId })
    .populate('members', 'login')
    .populate('lastMessage.message')
    .populate('lastMessage.sender', 'login')
    .exec((err, chats) => {
      if (err) {
        return res.status(500).send(err);
      }

      res.json(chats);
    });
};
