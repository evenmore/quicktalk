const chats = require('express').Router();
const createChat = require('./createChat');
const joinChat = require('./joinChat');
const leaveChat = require('./leaveChat');
const getAllChatsForUser = require('./getAllChatsForUser');
const messages = require('./messages');

chats.get('/', getAllChatsForUser);
chats.post('/', createChat);
chats.post('/:invitation', joinChat);
chats.delete('/:chatId', leaveChat);
chats.use('/', messages);

module.exports = chats;
