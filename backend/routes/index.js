const routes = require('express').Router();
const users = require('./users');
const chats = require('./chats');

routes.use('/users', users);
routes.use('/chats', chats);

module.exports = routes;
