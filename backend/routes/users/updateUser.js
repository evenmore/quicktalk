const User = require('../../../backend/models/user');

module.exports = (req, res) => {
  User.findById(req.params.userId, (err, user) => {
    if (err) {
      res.send(err);
    }

    user.save({
      login: req.body.login,
      password: req.body.password,
    }, (error) => {
      if (error) {
        return res.send(error);
      }
      res.json({ message: 'User updated!' });
    });
  });
};
