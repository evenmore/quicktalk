const User = require('../../../backend/models/user');

module.exports = (req, res) => {
  User.findById(req.params.userId, (err, user) => {
    if (err) {
      return res.send(err);
    }
    res.json(user);
  });
};
