const User = require('../../../backend/models/user');

module.exports = (req, res) => {
  User.remove({
    _id: req.params.userId,
  }, (err) => {
    if (err) {
      return res.send(err);
    }
    res.json({ message: 'Successfully deleted' });
  });
};
