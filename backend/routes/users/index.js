const users = require('express').Router();
const getAll = require('./getAllUsers');
const getSingle = require('./getSingleUser');
const updateUser = require('./updateUser');
const deleteUser = require('./deleteUser');

users.get('/', getAll);
users.get('/:userId', getSingle);
users.put('/:userId', updateUser);
users.delete('/:userId', deleteUser);

module.exports = users;
