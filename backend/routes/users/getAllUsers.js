const User = require('../../../backend/models/user');

module.exports = (req, res) => {
  User.find((err, users) => {
    if (err) {
      return res.send(err);
    }
    res.json(users);
  });
};
