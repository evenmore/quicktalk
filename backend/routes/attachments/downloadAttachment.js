module.exports = (req, res) => {
  const { attachmentId } = req.params;
  const Attachment = require('../../server').gridfs.model; // TODO: refactor if possible

  const stream = Attachment.readById(attachmentId);

  stream.on('error', (err) => {
    const errorMessage = 'Could not load attachment: ' + err;
    console.error(errorMessage);
    res.status(500).send(errorMessage);
  });

  stream.on('data', (data) => res.write(data));
  stream.on('close', () => res.end());
};