const attachments = require('express').Router();
const multer = require('multer');
const VerifyToken = require('../../auth/VerifyToken');
const uploadAttachments = require('./uploadAttachments');
const downloadAttachment = require('./downloadAttachment');

const storage = multer.memoryStorage();
const upload = multer({ storage });

attachments.get('/:attachmentId', downloadAttachment);
attachments.post('/', VerifyToken, upload.array('files', 10), uploadAttachments);

module.exports = attachments;