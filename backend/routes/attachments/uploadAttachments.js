const streamifier = require('streamifier');

module.exports = (req, res) => {
  const { files } = req;
  const Attachment = require('../../server').gridfs.model; // TODO: refactor if possible

  if (!files.length) {
    return res.end();
  }

  const fileIds = [];

  files.forEach(file => {
    const attachment = new Attachment({
      filename: file.originalname,
      contentType: file.mimetype
    });
    attachment.write(streamifier.createReadStream(file.buffer), (error, savedAttachment) => {
      if (error) {
        return res.status(500).send('Could not upload attachment');
      }

      fileIds.push(savedAttachment.id);

      if (fileIds.length === files.length) {
        res.json(fileIds);
      }
    });
  });
};