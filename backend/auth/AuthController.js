const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');
const User = require('../../backend/models/user');
const VerifyToken = require('./VerifyToken');

const router = express.Router();

router.use(express.urlencoded({ extended: false }));
router.use(express.json());

function createToken(userId) {
  return jwt.sign({ id: userId }, config.SECRET_TOKEN, {
    expiresIn: 86400, // expires in 24 hours
  });
}

router.post('/register', (req, res) => {
  const hashedPassword = bcrypt.hashSync(req.body.password, 8);

  User.create({
    login: req.body.login,
    password: hashedPassword,
  },
  (err, user) => {
    if (err) {
      return res.status(500).send('There was a problem registering the user.');
    }

    const token = createToken(user._id);

    res.status(200).send({ token });
  });
});

router.get('/me', VerifyToken, (req, res) => {
  User.findById(req.userId, { password: 0 }, (err, user) => {
    if (err) {
      return res.status(500).send('There was a problem finding the user.');
    }
    if (!user) {
      return res.status(404).send('No user found.');
    }

    res.status(200).send(user);
  });
});

router.post('/login', (req, res) => {
  User.findOne({ login: req.body.login }, (err, user) => {
    if (err) {
      return res.status(500).send('Error on the server.');
    }
    if (!user) {
      return res.status(404).send('No user found.');
    }

    const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) {
      return res.status(401).send({ token: null });
    }

    const token = createToken(user._id);

    res.status(200).send({ token });
  });
});

module.exports = router;
