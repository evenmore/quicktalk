module.exports = {
  SECRET_TOKEN: process.env.SECRET_TOKEN || 'secret',
  CLIENT_URL: process.env.CLIENT_URL || 'http://localhost:3000',
  DATABASE_HOST_URL: process.env.DATABASE_HOST_URL || 'localhost:27017',
  DATABASE_NAME: process.env.DATABASE_NAME || 'quicktalkdb'
};
