const jwt = require('jsonwebtoken');
const config = require('../../config');

function verifySocketToken(socket, next) {
  const token = socket.handshake.query.token;
  jwt.verify(token, config.SECRET_TOKEN, (err, decoded) => {
    if (err) {
      return next(new Error('authentication error'));
    }
    socket.handshake.query.userId = decoded.id;
    next();
  });
}

module.exports = verifySocketToken;
