
// holds userId -> [socketId] mapping, allowing to send socket message to needed user
const registry = {};

function socketRegistryMiddleware(socket, next) {
  const { userId } = socket.handshake.query;

  if (!registry[userId]) {
    registry[userId] = [];
  }

  console.log(`Add user = '${userId}' socket to registry (${Object.keys(registry).length} users in total)`);
  console.log(`User = '${userId}' has ${registry[userId].length + 1} open sockets`);
  registry[userId].push(socket.id);

  socket.on('disconnect', () => {
    console.log(`Remove user = '${userId}' socket from registry (${Object.keys(registry).length - 1} users left)`);
    console.log(`User = '${userId}' has ${Math.max(registry[userId].length - 1, 0)} open sockets`);
    const registryEntry = registry[userId];
    const socketIndex = registryEntry.indexOf(socket.id);
    if (socketIndex >= 0) {
      registryEntry.splice(socketIndex, 1);
      if (!registryEntry.length) {
        delete registry[userId];
      }
    }
  });
  next();
}

module.exports = {
  socketRegistryMiddleware,
  socketRegistry: registry
};
