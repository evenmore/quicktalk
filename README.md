## Quick Talk app

![Demo](demo.gif)

- Start database by running "docker-compose up" from project root folder
- From "backend" folder run "yarn" and then "yarn start" command to get backend running
- Do the same for "client" part
- _*(optional)_ Don't forget to create "SECRET_TOKEN" environment variable to make authentication works correctly
