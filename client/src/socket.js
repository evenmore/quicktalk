import io from 'socket.io-client';
import { BASE_URL } from './config';
import state from './state/store';
import {NEW_MESSAGE_RECEIVED} from './state/actions/action-types';

let socket;

export function connectSocket(token) {
  socket = io.connect(BASE_URL, {
    query: { token }
  });

  socket.on('connect', () => {
    console.log('Successfully connected to Socket!');
  });

  socket.on('new-message', (msg) => {
    console.log(`Received a new message via socket: ${JSON.stringify(msg)}`);
    state.dispatch({
      type: NEW_MESSAGE_RECEIVED,
      payload: msg
    });
  });
}

export function closeSocket() {
  if (socket && socket.connected) {
    console.log('Close socket');
    socket.close();
    socket = null;
  }
}
