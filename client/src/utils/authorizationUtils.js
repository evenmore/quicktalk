import axios from 'axios';
import store from '../state/store';
import {AUTHENTICATED} from '../state/actions/action-types';
import { connectSocket } from '../socket';

const user = JSON.parse(localStorage.getItem('user'));

if(user && user.token) {
  store.dispatch({
    type: AUTHENTICATED,
    payload: user.login
  });
  setAuthorizationHeader(user.token);
  connectSocket(user.token);
}

export default function setAuthorizationHeader (token) {
  if (token) {
    axios.defaults.headers.common['Authorization'] = JSON.stringify(token).replace(/"/g, "");
  } else {
    delete axios.defaults.headers.common.authorization;
  }
}