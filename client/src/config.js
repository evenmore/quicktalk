import axios from 'axios';

export const BASE_URL = 'http://localhost:3001';

axios.defaults.baseURL = BASE_URL + '/api';
