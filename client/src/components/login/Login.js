import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../state/actions/auth';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import {TextField} from 'redux-form-material-ui';
import styles from './Login.css';
import errorMessage from '../errors';

const required = value => (value == null ? 'Required' : undefined);

class Login extends Component {

    componentWillUnmount() {
        this.props.clearAuthErrorMessages();
    }

    submit = (values) => {
        this.props.loginAction(values);
    };

    render() {
        const { handleSubmit } = this.props;
        return (
            <div className="form">
                <div className={styles.container}>
                    <h1>Sign In</h1>
                    {errorMessage(this.props)}
                    <form onSubmit={ handleSubmit(this.submit) }>
                        <div>
                        <Field name="login"
                               component={TextField}
                               type="text"
                               placeholder="Login"
                               validate={required}
                        />
                            </div>
                        <div>
                        <Field name="password"
                               component={TextField}
                               type="password"
                               placeholder="Password"
                               validate={required}
                        />
                        </div>
                        <Button variant="contained" color="primary" type="submit" className={styles.signInBtn}>
                            Sign In
                        </Button>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { errorMessage: state.auth.error };
}

const reduxFormLogin = reduxForm({
    form: 'login'
})(Login);

export default connect(mapStateToProps, actions)(reduxFormLogin);