import React from 'react';

export default function errorMessage(props) {
    if (props.errorMessage) {
        return (
            <div className="authError">
                {props.errorMessage}
            </div>
        );
    }
}