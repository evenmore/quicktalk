import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import styles from './Navbar.css'
import Button from "@material-ui/core/es/Button/Button";

class Navbar extends Component {
    navbarLinks() {
        if (this.props.authenticated) {
            return [
                <Link key={'logout'} className={styles.simpleLink} to="/logout"><Button key="logout" color="inherit">Logout</Button></Link>
            ];
        }
        return [
            <Link key={'sign-in'} className={styles.simpleLink} to="/login"><Button key="login" color="inherit">Sign In</Button></Link>,
            <Link key={'sign-up'} className={styles.simpleLink} to="/registration"><Button key="registration" color="inherit">Sign Up</Button></Link>
        ];
    }

    render() {
        let welcomeDiv = <div></div>;
        if (this.props.login) {
            welcomeDiv = <div className={styles.username}>Welcome {this.props.login}!</div>
        }

        return (
            <AppBar position="static" className={styles.header}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" className={styles.grow}>
                        <Link className={styles.simpleLink} to="/chats"><span className="brand">QuickTalk</span></Link>
                    </Typography>
                    {welcomeDiv}
                    {this.navbarLinks()}
                </Toolbar>
            </AppBar>
        );
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated,
        login: state.auth.login
    };
}

export default connect(mapStateToProps)(Navbar);