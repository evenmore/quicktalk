import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../state/actions/auth';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {TextField} from "redux-form-material-ui";
import Button from '@material-ui/core/Button';
import styles from './Registration.css';
import errorMessage from '../errors';

const required = value => (value == null ? 'Required' : undefined);

class Registration extends Component {
    constructor(props) {
        super(props);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    componentWillUnmount() {
        this.props.clearAuthErrorMessages();
    }

    handleFormSubmit(formProps) {
        this.props.registrationAction(formProps);
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <div className={styles.container}>
                <h1>Sign up</h1>
                {errorMessage(this.props)}
                <form onSubmit={handleSubmit(this.handleFormSubmit)}>

                    <div><Field name="login" component={TextField} type="text" placeholder="Login" validate={required}/></div>
                    <div><Field name="password" component={TextField} type="password" placeholder="Password" validate={required}/></div>

                    <Button  variant="contained" color="primary" type="submit" className={styles.signUpBtn}>Sign up</Button>

                    <div className="form-bottom">
                        <p>Already signed up?</p>
                        <Link to="/login">Click here to sign in</Link>
                    </div>
                </form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { errorMessage: state.auth.error };
}


const reduxFormRegistration = reduxForm({
    form: 'registration'
})(Registration);

export default connect(mapStateToProps, actions)(reduxFormRegistration);