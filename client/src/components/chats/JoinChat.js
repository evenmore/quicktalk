import React, {Component} from "react";
import {connect} from "react-redux";
import {joinChat} from "../../state/actions/chat";
import {Redirect} from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";

class JoinChat extends Component {

  componentDidMount() {
    this.props.joinChat(this.props.match.params.invitation);
  }

  render() {
    if (this.props.joinedChat) {
      return (
        <Redirect to={'/chats/' + this.props.joinedChat}/>
      );
    } else if (this.props.chatJoinError) {
      return (
        <Grid container justify = "center">
          Could not join the chat.
        </Grid>
      );
    }
    return (
      <Grid container justify = "center">
        <CircularProgress />
      </Grid>
    );
  }
}

export default connect(
  (state) => ({
    joinedChat: state.chat.joinedChat,
    chatJoinError: state.chat.chatJoinError
  }),
  { joinChat }
)(JoinChat);
