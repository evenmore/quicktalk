import React, {Component} from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import CreateNewChatModal from './CreateNewChatModal';
import * as actions from '../../state/actions/chat';
import ChatMessages from './messages/ChatMessages';
import Grid from '@material-ui/core/Grid';
import ChatList from './ChatList';
import {history} from '../../state/store';
import Hidden from '@material-ui/core/Hidden';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  container: {
    height: '93%'
  },
  chatsContainer: {
    height: '100%',
    position: 'relative'
  },
  newChat: {
    height: '5%'
  }
});

class Chats extends Component {

  componentDidMount() {
    this.props.getAllChatsForUser();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const chatId = this.props.match.params.chatId;
    if (prevProps.chats !== this.props.chats) {
      if (this.props.selectedChat && this.props.selectedChat._id === chatId) {
        return;
      }
      this.props.selectChat(chatId);
    } else if (prevProps.match.params.chatId !== chatId) {
      this.props.selectChat(chatId);
    }
  }

  addNewChat = () => {
    this.props.openCreateChatView();
  };

  closeNewChatModal = () => {
    this.props.closeCreateChatView();
  };

  loadMoreMessages = () => {
    if (this.props.allMessagesLoaded) {
      return;
    }

    const messageTime = this.props.selectedChatMessages.length ? this.props.selectedChatMessages[0].time : null;
    this.props.loadMoreMessages(this.props.match.params.chatId, messageTime);
  };

  sendMessage = (message) => {
    this.props.sendMessage(this.props.match.params.chatId, message);
  };

  leaveFromChat = () => {
    this.props.leaveFromChat(this.props.match.params.chatId);
  };

  selectChat = (chatId) => {
    history.push('/chats/' + chatId);
  };

  hideChatsView = () => {
    return !this.hideMessagesView();
  };

  hideMessagesView = () => {
    return !this.props.match.params.chatId;
  };

  render() {
    const {classes} = this.props;
    return (
      <Grid container className={classes.container}>
        <Hidden xsDown={this.hideChatsView()}>
          <Grid item xs={12} sm={5} md={3} className={classes.chatsContainer}>
            <Grid container justify="center" className={classes.newChat}>
              <Button onClick={this.addNewChat} variant="contained" color="primary" className={classes.button}>
                New Chat
              </Button>
            </Grid>

            <ChatList chatList={this.props.chats}
                      loadingError={this.props.chatsLoadingError}
                      selectChat={this.selectChat}
                      selectedChat={this.props.selectedChat}/>
          </Grid>
        </Hidden>

        <Hidden xsDown={this.hideMessagesView()}>
          <Grid item xs={12} sm={7} md={9}>
            <ChatMessages chat={this.props.selectedChat}
                          messages={this.props.selectedChatMessages}
                          allLoaded={this.props.allMessagesLoaded}
                          loadingError={this.props.selectedChatMessagesError}
                          loadMore={this.loadMoreMessages}
                          sendMessage={this.sendMessage}
                          leaveFromChat={this.leaveFromChat}/>
          </Grid>
        </Hidden>

        <CreateNewChatModal open={this.props.createNewChatModelOpen}
                            invitation={this.props.invitation}
                            createChat={this.props.createChat}
                            close={() => this.closeNewChatModal()}/>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    invitation: state.chat.newChat.invitation,
    createNewChatModelOpen: state.chat.createNewChatModelOpen,
    selectedChat: state.chat.selectedChat,
    selectedChatMessages: state.chat.messages,
    selectedChatMessagesError: state.chat.messageLoadingError,
    allMessagesLoaded: state.chat.allMessagesLoaded,
    chats: state.chat.chats,
    chatsLoadingError: state.chat.chatsLoadingError
  };
};

export default withStyles(styles)(connect(
  mapStateToProps,
  actions
)(Chats));