import React, {Component} from 'react';
import {withStyles} from '@material-ui/core';
import Grid from '@material-ui/core/Grid/Grid';
import Typography from '@material-ui/core/es/Typography/Typography';
import Chat from './Chat.js';

const styles = theme => ({
  chooseChat: {
    paddingTop: '30%'
  },
  scrollable: {
    overflowY: 'scroll',
    maxHeight: '93%'
  }
});


class ChatList extends Component {

  render() {
    const {classes} = this.props;
    const {selectedChat} = this.props;
    let chats = null;

    if (this.props.loadingError) {
      chats = (
        <Grid container justify="center" alignItems="center">
          <Typography variant="body1" className={classes.chooseChat} color="error">Could not load any
            chats!</Typography>
        </Grid>
      );
    } else if (this.props.chatList) {
      chats = (
        this.props.chatList.map((chat) =>
          (
            <div key={chat._id}>
              <Chat chat={chat} selectChat={this.props.selectChat}
                    selected={selectedChat && selectedChat._id === chat._id}/>
            </div>
          )
        )
      );
    }
    return <div className={classes.scrollable}>{chats}</div>;
  }
}

export default withStyles(styles)(ChatList);