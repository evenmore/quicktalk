import React, {Component} from 'react';
import Typography from '@material-ui/core/es/Typography/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {withStyles} from '@material-ui/core';
import CardHeader from '@material-ui/core/CardHeader';
import blue from '@material-ui/core/colors/blue';

const styles = theme => ({
  card: {
    margin: theme.spacing.unit,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: blue[200]
    }
  },
  selected: {
    backgroundColor: blue[500] + '!important'
  }
});

const MAX_MESSAGE_LENGTH = 50;

class Chat extends Component {

  lastMessage = () => {
    const lastMessage = this.props.chat.lastMessage;
    if (lastMessage) {
      let result = lastMessage.sender.login + ': ';

      const content = lastMessage.message.content;

      if (content.length > MAX_MESSAGE_LENGTH) {
        result += content.substring(0, MAX_MESSAGE_LENGTH) + '...';
      } else {
        result += content;
      }

      return result;
    }

    return 'No messages here yet';
  };

  render() {
    const {classes} = this.props;
    const {chat} = this.props;
    const {selected} = this.props;
    const selectedChatStyle = selected ? classes.selected : null;

    return (
      <Card className={classes.card + ' ' + selectedChatStyle} onClick={() => this.props.selectChat(chat._id)}>
        <CardHeader title={chat.name}/>
        <CardContent>
          <Typography className={classes.title} color="textSecondary">
            {this.lastMessage()}
          </Typography>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(Chat);