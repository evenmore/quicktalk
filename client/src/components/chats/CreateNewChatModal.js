import React, {Component} from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';

class CreateNewChatModal extends Component {

  handleClose = () => {
    this.props.close();
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.createChat(this.state.chatName);
  };

  handleNameInputChange = (e) => {
    this.setState({ chatName: e.target.value });
  };

  getJoinChatLink = () => {
    return window.location.origin + '/chats/' + this.props.invitation + '/join'
  };

  render() {
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add a new Chat</DialogTitle>

          <form onSubmit={this.handleSubmit}>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Chat name"
                type="text"
                fullWidth
                required={true}
                onChange={this.handleNameInputChange}
              />

              { this.props.invitation ?
                <DialogContentText>
                  <br/>
                  You can share this link with everyone who wants to join a chat with you
                  <br/>
                  <a href={this.getJoinChatLink()}>{this.getJoinChatLink()}</a>
                </DialogContentText>
                : ''
              }
            </DialogContent>

            { this.props.invitation ?
              (
                <DialogActions>
                  <Button onClick={this.handleClose} color="primary">
                    Close
                  </Button>
                </DialogActions>
              ) : (
                <DialogActions>
                  <Button onClick={this.handleClose} color="primary">
                    Cancel
                  </Button>
                  <Button color="primary" type="submit">
                    Ok
                  </Button>
                </DialogActions>
              )
            }
          </form>

        </Dialog>
      </div>
    );
  }
}

export default CreateNewChatModal;