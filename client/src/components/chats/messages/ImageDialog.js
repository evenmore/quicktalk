import React, {Component} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {withStyles} from '@material-ui/core';

const styles = theme => ({
  imgPreview: {
    width: '100%'
  }
});

class ImageDialog extends Component {

  handleClose = () => {
    this.props.close();
  };

  render() {
    const { classes } = this.props;

    return (
      <Dialog
        open={this.props.open}
        onClose={this.handleClose}
        aria-labelledby="image preview"
      >
        <DialogContent>
          <img className={classes.imgPreview} src={this.props.img} alt=""/>
        </DialogContent>
      </Dialog>
    );
  }

}

export default withStyles(styles)(ImageDialog);