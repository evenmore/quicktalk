import React, {Component} from 'react';
import Typography from '@material-ui/core/es/Typography/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {withStyles} from '@material-ui/core';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import Moment from 'react-moment';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { BASE_URL } from '../../../config';
import ImageDialog from './ImageDialog';

const styles = theme => ({
  card: {
    margin: theme.spacing.unit
  },
  gridList: {
  }
});

class ChatMessage extends Component {
  state = {
    previewOpen: false,
    previewImg: ''
  };

  openPreview = (img) => {
    this.setState({
      previewOpen: true,
      previewImg: img
    });
  };

  closePreview = () => {
    this.setState({ previewOpen: false });
  };

  render() {
    const { classes } = this.props;
    const { message } = this.props;

    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="Sender" style={{ backgroundColor: this.props.avatarColor }}>
              {message.sender.login.substr(0, 1).toUpperCase()}
            </Avatar>
          }
          title={message.sender.login}
          subheader={
            <Moment date={message.time} format="D MMM YYYY, hh:mm:ss"/>
          }
        />
        <CardContent>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            {message.content}
          </Typography>

          <GridList cellHeight={160} className={classes.gridList} cols={3}>
            {message.attachments.map(attachment => (
              <GridListTile key={message._id + ':' + attachment} cols={1}>
                <img src={BASE_URL + '/api/attachments/' + attachment} alt="attachment"
                     onClick={() => this.openPreview(BASE_URL + '/api/attachments/' + attachment)} />
              </GridListTile>
            ))}
          </GridList>

          <ImageDialog img={this.state.previewImg} open={this.state.previewOpen} close={this.closePreview}/>
        </CardContent>
      </Card>
    );
  }

}

export default withStyles(styles)(ChatMessage);