import React, {Component} from 'react';
import Paper from '@material-ui/core/es/Paper';
import {withStyles} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/es/Typography/Typography';
import ChatMessage from './ChatMessage';
import randomColor from 'random-material-color';
import MessageInputControl from './MessageInputControl';
import Button from "@material-ui/core/Button/Button";

const styles = theme => ({
  container: {
    height: '100%',
    position: 'relative'
  },
  chip: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  header: {
    paddingLeft: theme.spacing.unit,
    paddingTop: theme.spacing.unit,
  },
  headerContainer: {
  },
  chooseChat: {
    paddingTop: '30%'
  },
  messages: {
    marginTop: theme.spacing.unit,
    overflowY: 'scroll',
    maxHeight: '85%'
  },
  messageInput: {
    height: 'auto',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0
  },
  button: {
    margin: theme.spacing.unit
  }
});

class ChatMessages extends Component {

  constructor(props) {
    super(props);

    this.avatarColors = {};
    this.state = {
      loadingMore: false,
      messageSent: false,
      chatBottom: false
    };
  }

  shouldComponentUpdate() {
    this.previousFirstMessageEl = this.firstMessageEl;
    return true;
  }

  componentDidUpdate(prevProps) {
    if (this.props.messages.length && !prevProps.messages.length) {
      return this.scrollToBottom();
    }

    if (this.props.messages.length !== prevProps.messages.length) {
      if (this.state.messageSent) {
        this.setState({
          messageSent: false,
          loadingMore: false
        });
        return this.scrollToBottom();
      }

      if (this.state.loadingMore) {
        this.setState({ loadingMore: false });
        return this.scrollToMessage();
      }

      if (this.state.chatBottom) {
        return this.scrollToBottom();
      }
    }
  }

  scrollToMessage = () => {
    if (this.previousFirstMessageEl) {
      this.previousFirstMessageEl.scrollIntoView();
    }
  };

  getColor = (login) => {
    if (this.avatarColors[login]) {
      return this.avatarColors[login];
    }

    this.avatarColors[login] = randomColor.getColor();
    return this.avatarColors[login];
  };

  scrollToBottom = () => {
    if (this.chatBottomEl) {
      this.chatBottomEl.scrollIntoView();
    }
  };

  messagesScrollHandler = (e) => {
    if (e.target.scrollTop === 0 && e.target.scrollHeight > 0) {
      if (this.props.allLoaded) {
        return this.setState({ loadingMore: false });
      }
      this.setState({ loadingMore: true });
      this.props.loadMore();
    }

    const bottomScrollOffset = 100;
    if (e.target.scrollHeight - e.target.scrollTop <= e.target.clientHeight + bottomScrollOffset) {
        this.setState({ chatBottom: true });
    } else {
        this.setState({ chatBottom: false });
    }
  };

  sendMessage = (msg) => {
    this.setState({ messageSent: true });
    this.props.sendMessage(msg);
  };

  getJoinChatLink = () => {
    return window.location.origin + '/chats/' + this.props.chat.invitation + '/join'
  };

  leaveFromChat = () => {
      this.props.leaveFromChat();
  };

  render() {
    const { classes } = this.props;

    if (!this.props.chat) {
      return (
        <Paper className={classes.container} square={true}>
          <Grid container justify="center" alignItems="center">
            <Typography variant="subtitle1" className={classes.chooseChat}>Choose a Chat</Typography>
          </Grid>
        </Paper>
      );
    }

    let messages = null;
    if (this.props.loadingError) {
      messages = (
        <Grid container justify="center" alignItems="center">
          <Typography variant="body1" className={classes.chooseChat} color="error">Could not load chat messages!</Typography>
        </Grid>
      );
    } else if (this.props.messages) {
      messages = (
        this.props.messages.map((message, index) =>
          index === 0
            ? (
              <div key={message._id} ref={el => this.firstMessageEl = el}>
                <ChatMessage message={message} avatarColor={this.getColor(message.sender.login)}/>
              </div>
            ) : (
              <div key={message._id}>
                <ChatMessage message={message} avatarColor={this.getColor(message.sender.login)}/>
              </div>
            )
        )
      );
    }

    return (
      <Paper className={classes.container} square={true}>
        <Paper square={true} className={classes.headerContainer}>
          <Grid container>
            <Grid item xs={4}>
              <Typography variant="h6" className={classes.header}>{this.props.chat.name}</Typography>
              <span>(Invitation: <a href={this.getJoinChatLink()}>{this.props.chat.invitation}</a>)</span>
            </Grid>

            <Grid container item xs={8} justify="flex-end">
              <Button onClick={this.leaveFromChat} variant="text" color="secondary" className={classes.button}>
                  Leave from a chat
              </Button>
              {
                this.props.chat.members.map(member =>
                  <Chip key={member.login} label={member.login} className={classes.chip} style={{ backgroundColor: this.getColor(member.login) }}/>
                )
              }
            </Grid>
          </Grid>
        </Paper>

        <div className={classes.messages} onScroll={this.messagesScrollHandler}>
          {
            this.props.allLoaded && (
              <Typography color="textSecondary" align="center" gutterBottom>All messages loaded</Typography>
            )
          }
          { messages }
          <div ref={el => this.chatBottomEl = el}/>
        </div>

        <div className={classes.messageInput}>
          <MessageInputControl sendMessage={this.sendMessage}/>
        </div>
      </Paper>
    );
  }
}

export default withStyles(styles)(ChatMessages);