import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import {withStyles} from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Send from '@material-ui/icons/Send';
import AttachFile from '@material-ui/icons/AttachFile';
import Paper from '@material-ui/core/es/Paper/Paper';
import {DropzoneDialog} from 'material-ui-dropzone';

const styles = theme => ({
  root: {
    height: '100%',
    width: '100%'
  },
  textField: {
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    width: '85%',
  }
});

class MessageInputControl extends Component {

  constructor(props) {
    super(props);

    this.state = {
      message: '',
      files: [],
      uploadModalOpen: false
    };
  }

  handleChange = (e) => {
    this.setState({ message: e.target.value })
  };

  handleSubmit = () => {
    if (this.state.message || this.state.files) {
      this.props.sendMessage({
        message: this.state.message,
        files: this.state.files
      });

      this.setState({
        message: '',
        files: []
      });
    }
  };

  handleKeyPress = (e) => {
    if ((e.ctrlKey || e.metaKey) && (e.keyCode === 13 || e.keyCode === 10)) {
      this.handleSubmit();
    }
  };

  handleFilesUpload = (files) => {
    this.setState({
      uploadModalOpen: false,
      files
    });
  };

  handleOpenUploadDialog = () => {
    this.setState({ uploadModalOpen: true });
  };

  handleCloseUploadDialog = () => {
    this.setState({
      uploadModalOpen: false
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <Paper square={true} className={classes.root}>
        <IconButton
          aria-label="Upload"
          onClick={this.handleOpenUploadDialog}
        >
          <AttachFile/>
        </IconButton>
        <TextField
          id="messageTextArea"
          className={classes.textField}
          placeholder="Write a message..."
          multiline={true}
          autoFocus={true}
          rowsMax={10}
          margin="normal"
          value={this.state.message}
          onChange={this.handleChange}
          onKeyDown={this.handleKeyPress}
          InputProps={{
            endAdornment:
              <InputAdornment position="end">
                <IconButton
                  disabled={!this.state.message && !this.state.files}
                  aria-label="Send message"
                  onClick={this.handleSubmit}
                >
                  <Send/>
                </IconButton>
              </InputAdornment>,
          }}
        />
        <DropzoneDialog
          open={this.state.uploadModalOpen}
          filesLimit={10}
          onSave={this.handleFilesUpload}
          showPreviews={true}
          onClose={this.handleCloseUploadDialog}
        />
      </Paper>
    );
  }

}

export default withStyles(styles)(MessageInputControl);