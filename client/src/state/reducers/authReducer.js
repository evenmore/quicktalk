import {
  AUTHENTICATED,
  UNAUTHENTICATED,
  AUTHENTICATION_ERROR,
  SIGNUP_FAILURE,
  WITHOUT_AUTH_ERRORS
} from '../actions/action-types';

const initState = {
  authenticated: false
};

export default function (state = initState, action) {
  switch (action.type) {
    case AUTHENTICATED:
      return {...state, authenticated: true, login: action.payload};
    case UNAUTHENTICATED:
      return {...state, authenticated: false, login: undefined};
    case AUTHENTICATION_ERROR:
      return {...state, error: action.payload};
    case SIGNUP_FAILURE:
      return {...state, error: action.payload};
    case WITHOUT_AUTH_ERRORS:
      return {...state, error: undefined};
    default:
      return state;
  }
}