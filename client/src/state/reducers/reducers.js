import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { reducer as form } from 'redux-form';
import authReducer from './authReducer';
import chatReducer from './chatReducers';

const initialState = {
};

function app(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default (history) => combineReducers({
  router: connectRouter(history),
  app,
  form,
  auth: authReducer,
  chat: chatReducer
})