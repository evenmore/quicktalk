import {
  CHAT_CREATED,
  CHAT_JOIN_ERROR,
  CHAT_JOINED,
  CHAT_MESSAGES_LOADED,
  CHAT_MESSAGES_LOADING_ERROR,
  CLOSE_CREATE_CHAT, NEW_MESSAGE_RECEIVED,
  OPEN_CREATE_CHAT,
  SELECT_CHAT,
  CHAT_LEFT,
  CHATS_LOADED, CHATS_LOADING_ERROR
} from '../actions/action-types';

const initialState = {
  newChat: {},
  createNewChatModelOpen: false,
  chats: [],
  selectedChat: undefined,
  messages: [],
  messageLoadingError: undefined,
  allMessagesLoaded: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case OPEN_CREATE_CHAT:
      return {...state, newChat: {}, createNewChatModelOpen: true};
    case CLOSE_CREATE_CHAT:
      return {...state, createNewChatModelOpen: false};
    case CHAT_CREATED:
      return {...state, newChat: action.payload, chats: [...state.chats, action.payload]};
    case CHAT_JOINED:
      return {...state, joinedChat: action.payload, chatJoinError: undefined};
    case CHAT_JOIN_ERROR:
      return {...state, chatJoinError: action.payload, joinedChat: undefined};
    case CHATS_LOADED:
      return {...state, chats: action.payload, chatsLoadingError: undefined};
    case CHATS_LOADING_ERROR:
      return {...state, chatsLoadingError: action.payload};
    case CHAT_LEFT:
      const chatToRemove = state.selectedChat;
      const newChats = state.chats.filter(chat => chat._id !== chatToRemove._id);
      return {...state, selectedChat: undefined, chats: newChats};
    case SELECT_CHAT:
      const chatId = action.payload;
      const selectedChat = chatId ? state.chats.find(chat => chat._id === chatId) : undefined;
      return {...state, selectedChat, messages: [], allMessagesLoaded: false};
    case CHAT_MESSAGES_LOADED:
      return {
        ...state,
        messages: [...action.payload, ...state.messages],
        allMessagesLoaded: !action.payload.length,
        messageLoadingError: undefined
      };
    case CHAT_MESSAGES_LOADING_ERROR:
      return {...state, messageLoadingError: action.payload, messages: []};
    case NEW_MESSAGE_RECEIVED:
      const newMessage = action.payload;
      const foundChatForMessage = state.chats.find(chat => chat._id === newMessage.chat);

      if (!foundChatForMessage) {
        return state;
      }

      const chatForMessage = {...foundChatForMessage};

      if (!chatForMessage.lastMessage) {
        chatForMessage.lastMessage = {};
      }

      chatForMessage.lastMessage.message = newMessage;
      chatForMessage.lastMessage.sender = newMessage.sender;

      const updatedChats = state.chats.filter(chat => chat._id !== newMessage.chat);
      updatedChats.unshift(chatForMessage);

      const newState = {...state, chats: updatedChats};

      if (state.selectedChat && state.selectedChat._id === newMessage.chat) {
        newState.messages = [...state.messages, newMessage];
      }

      return newState;
    default:
      return state;
  }
}