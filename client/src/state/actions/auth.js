import axios from 'axios';
import * as actionTypes from './action-types';
import {history} from '../store';
import setAuthorizationHeader from '../../utils/authorizationUtils';
import {closeSocket, connectSocket} from '../../socket';

export function loginAction({login, password}) {
  return async (dispatch) => {
    try {
      const res = await axios.post(`/auth/login`, {login, password});
      dispatch({
        type: actionTypes.AUTHENTICATED,
        payload: login
      });
      localStorage.setItem('user', JSON.stringify({
        login: login,
        token: res.data.token
      }));
      setAuthorizationHeader(res.data.token);
      connectSocket(res.data.token);
      history.push('/chats');
    } catch (error) {
      dispatch({
        type: actionTypes.AUTHENTICATION_ERROR,
        payload: 'Invalid login or password'
      });
    }
  };
}

export function clearAuthErrorMessages() {
  return {
    type: actionTypes.WITHOUT_AUTH_ERRORS
  };
}

export function logoutAction() {
  localStorage.clear();
  setAuthorizationHeader();
  closeSocket();
  history.push('/');
  return {
    type: actionTypes.UNAUTHENTICATED
  };
}

export function registrationAction(props) {
  return async (dispatch) => {
    try {
      await axios.post(`/auth/register`, props);
      dispatch({type: actionTypes.SIGNUP_SUCCESS});
      history.push(`/login`);
    } catch (error) {
      dispatch({
        type: actionTypes.SIGNUP_FAILURE,
        payload: 'Seems like login already exists :('
      });
    }
  }
}
