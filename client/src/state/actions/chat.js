import axios from 'axios';
import {history} from '../store';
import {
  CHAT_CREATED,
  CHAT_JOIN_ERROR,
  CHAT_JOINED, CHAT_MESSAGES_LOADED, CHAT_MESSAGES_LOADING_ERROR,
  CLOSE_CREATE_CHAT, MESSAGE_SENDING_ERROR, MESSAGE_SENT,
  OPEN_CREATE_CHAT,
  SELECT_CHAT,
  CHAT_LEFT,
  CHATS_LOADED, CHATS_LOADING_ERROR
} from './action-types';

export function createChat(chatName) {
  return (dispatch) => {
    axios.post(`/chats`, {name: chatName})
      .then((resp) => dispatch({
        type: CHAT_CREATED,
        payload: resp.data
      }))
      .catch(err => console.error('Could not create chat. ' + err));
  };
}

export function joinChat(invitation) {
  return (dispatch) => {
    axios.post(`/chats/${invitation}`)
      .then((resp) => dispatch({
        type: CHAT_JOINED,
        payload: resp.data.chat
      }))
      .catch((err) => dispatch({
        type: CHAT_JOIN_ERROR,
        payload: err
      }));
  };
}

export function leaveFromChat(chatId) {
  return async (dispatch) => {
    try {
      await axios.delete(`/chats/${chatId}`);
      dispatch({
        type: CHAT_LEFT
      });
      history.push(`/chats`);
    } catch (err) {
      console.error('Could not leave from a chat' + err);
    }
  };
}

export function getAllChatsForUser() {
  return (dispatch) => {
    axios.get(`/chats`)
      .then((resp) => dispatch({
        type: CHATS_LOADED,
        payload: resp.data
      }))
      .catch((err) => dispatch({
        type: CHATS_LOADING_ERROR,
        payload: err
      }));
  }
}

export function selectChat(chatId) {
  return (dispatch) => {
    dispatch({
      type: SELECT_CHAT,
      payload: chatId
    });

    if (chatId) {
      loadChatMessages(dispatch, chatId);
    }
  };
}

export function loadMoreMessages(chatId, firstMessageTime) {
  return (dispatch) => {
    loadChatMessages(dispatch, chatId, firstMessageTime)
  };
}

function loadChatMessages(dispatch, chatId, firstMessageTime = new Date()) {
  axios.get(`/chats/${chatId}/messages`, {
    params: {
      time: firstMessageTime,
      limit: 20
    }
  })
    .then((resp) => dispatch({
      type: CHAT_MESSAGES_LOADED,
      payload: resp.data
    }))
    .catch((err) => dispatch({
      type: CHAT_MESSAGES_LOADING_ERROR,
      payload: err
    }));
}

function uploadAttachments(files) {
  if (files && files.length) {
    const formData = new FormData();
    files.forEach(file => formData.append('files', file));

    return axios.post(`/attachments`, formData).then(resp => resp.data);
  }

  return Promise.resolve([]);
}

export function sendMessage(chatId, message) {
  return (dispatch) => {
    uploadAttachments(message.files)
      .then((attachments) => {
        delete message.files;
        message.attachments = attachments;

        axios.post(`/chats/${chatId}/messages`, message)
          .then((resp) => dispatch({
            type: MESSAGE_SENT,
            payload: resp.data
          }))
          .catch((err) => dispatch({
            type: MESSAGE_SENDING_ERROR,
            payload: err
          }));
      });
  };
}

export function openCreateChatView() {
  return (dispatch) => {
    dispatch({
      type: OPEN_CREATE_CHAT
    })
  };
}

export function closeCreateChatView() {
  return (dispatch) => {
    dispatch({
      type: CLOSE_CREATE_CHAT
    })
  };
}
