export const AUTHENTICATED = 'authenticated_user';
export const UNAUTHENTICATED = 'unauthenticated_user';
export const AUTHENTICATION_ERROR = 'authentication_error';

export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_FAILURE = 'SIGNUP_FAILURE';

export const WITHOUT_AUTH_ERRORS = 'without_auth_errors';

export const OPEN_CREATE_CHAT = 'OPEN_CREATE_CHAT';
export const CLOSE_CREATE_CHAT = 'CLOSE_CREATE_CHAT';
export const CHAT_CREATED = 'CHAT_CREATED';

export const CHAT_JOINED = 'CHAT_JOINED';
export const CHAT_JOIN_ERROR = 'CHAT_JOIN_ERROR';

export const CHAT_LEFT = 'CHAT_LEFT';

export const CHATS_LOADED = 'CHATS_LOADED';
export const CHATS_LOADING_ERROR = 'CHATS_LOADING_ERROR';

export const SELECT_CHAT = 'SELECT_CHAT';
export const CHAT_MESSAGES_LOADED = 'CHAT_MESSAGES_LOADED';
export const CHAT_MESSAGES_LOADING_ERROR = 'CHAT_MESSAGES_LOADING_ERROR';

export const MESSAGE_SENT = 'MESSAGE_SENT';
export const MESSAGE_SENDING_ERROR = 'MESSAGE_SENDING_ERROR';
export const NEW_MESSAGE_RECEIVED = 'NEW_MESSAGE_RECEIVED';
