import React, {Component} from 'react';
import './App.css';
import {Route, Redirect} from 'react-router-dom';
import Chats from './components/chats/Chats';
import Login from './components/login/Login';
import Logout from './components/logout/Logout';
import Navbar from './components/navbar/Navbar';
import Registration from './components/registration/Registration';
import state from './state/store';
import JoinChat from './components/chats/JoinChat';

const PrivateRoute = ({component: Component, isAuthenticated, ...rest}) => {
  return <Route {...rest} render={(props) => {
    return isAuthenticated
      ? <Component {...props} />
      : <Redirect to='/login'/>
  }}/>
};

class App extends Component {

  isAuthenticated = () => {
    return state.getState().auth.authenticated;
  };

  render() {
    return (
      <div className="app">
        <Navbar/>
        <Route path="/login" component={Login}/>
        <Route path="/logout" component={Logout}/>
        <Route path="/registration" component={Registration}/>
        <PrivateRoute isAuthenticated={this.isAuthenticated()} path="/chats/:invitation/join"
                      component={JoinChat}/>
        <PrivateRoute isAuthenticated={this.isAuthenticated()} exact path="/chats/:chatId?"
                      component={Chats}/>
        <Route exact path="/" render={() => (<Redirect to="/chats"/>)}/>
      </div>
    );
  }
}

export default App;
